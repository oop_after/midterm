package com.thanawuth.midterm;

public class BluetoothSpeaker {
    String charge = "Bluetooth Speaker USB";
    String[] controls = { "power", "volume", "skip", "play/pause" };
    String color = "Black";
    String power = " ";
    int volume = 0;
    String playpause = " ";
    String skip = " ";

    public void powerOn() {
        power = "ON";
    }

    public void powerOff() {
        power = "OFF";
    }

    public void volumeUp() {
        volume++;
    }

    public void volumeDown() {
        volume--;
    }

    public void play() {
        playpause = "PLAYING...";
    }

    public void pause() {
        playpause = "PAUSE";
    }

    public void skipMusic() {
        skip = "SKIP>>";
    }

}
