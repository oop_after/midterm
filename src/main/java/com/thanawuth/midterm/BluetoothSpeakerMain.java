package com.thanawuth.midterm;

public class BluetoothSpeakerMain {
    public static void main(String[] args) {
        BluetoothSpeaker b = new BluetoothSpeaker();
        System.out.println(b.color);
        b.powerOn();
        System.out.println(b.power);
        b.volumeUp();
        b.volumeUp();
        b.volumeUp();
        b.volumeUp();
        System.out.println(b.volume);
        b.play();
        System.out.println(b.playpause);
        b.skipMusic();
        System.out.println(b.skip);
        System.out.println(b.charge);
        b.pause();
        System.out.println(b.playpause);

    }
}

